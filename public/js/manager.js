var $body = $('body');

var LarakitManager = {
    /**
     * Получить item  по кнопке
     * @param $btn
     * @returns {*}
     */
    getItem: function ($btn) {
        return $btn.closest('.js-item');
    },

    /**
     * Получить list
     * @param $btn
     * @returns {*}
     */
    getList: function ($btn) {
        return $btn.closest('.js-list');
    },

    /**
     * Получить entity списка
     * @param $btn
     * @returns string
     */
    getListKey: function ($btn) {
        return this.getList($btn).attr('data-key');
    },

    /**
     * Получить идентификатор элемента списка
     * @param $btn
     * @returns integer
     */
    getId: function ($btn) {
        return this.getItem($btn).attr('data-id');
    },

    /**
     * Получить название действия
     * @param $btn
     * @returns string
     */
    getAction: function ($btn) {
        return $btn.attr('data-action');
    },

    /**
     * Получить базовый URL модели
     * @param $btn
     * @returns string
     */
    getBaseUrl: function ($btn) {
        return this.getList($btn).attr('data-url');
    },

    /**
     * Получить ссылку на действие
     * @param $btn
     * @returns {string}
     */
    getActionLink: function ($btn) {
        return this.getBaseUrl($btn) + '/' + this.getId($btn) + '/' + this.getAction($btn);
    },

    /**
     * Получить название превьюшки
     * @param $btn
     * @returns string
     */
    getThumbName: function ($btn) {
        return $btn.attr('data-thumb-name');
    },

    /**
     * Получить размер превьюшки
     * @param $btn
     * @returns string
     */
    getThumbSize: function ($btn) {
        return $btn.attr('data-thumb-size');
    },

    markInited: function ($btn) {
        $btn.addClass('js-init');
    },

    getRowHtml: function ($list, model_data) {
        var row_type = $list.attr('data-row-type');
        //если не известен тип строки в листе
        if ('undefined' == typeof (row_type)) {
            return;
        }
        var item_html = model_data.rows[row_type];
        //если ожидаемый списокм тип строки не пришел
        if ('undefined' == typeof (item_html)) {
            return;
        }
        return item_html;
    },

    rowInsert: function ($list, model_data) {
        var insert_mode = $list.attr('data-insert-mode'),
            item_html = LarakitManager.getRowHtml($list, model_data);
        if (!item_html) {
            return;
        }
        if (insert_mode == 'append' || 'undefined' == typeof (insert_mode)) {
            $(item_html)
                .appendTo($list)
                .addClass('bounceInLeft flash animated');
        } else {
            $(item_html)
                .prependTo($list)
                .addClass('bounceInLeft flash animated');
        }
        var $item = $list.children('.js-item[data-id=' + model_data.id + ']');
        setTimeout(function () {
            $item
                .removeClass("bounceInLeft flash animated");
        }, 1000);
    },

    /**
     * Обновление существующей в списке строки
     * @param data
     */
    rowUpdate: function ($list, model_data) {
        var $item = $list.children('.js-item[data-id=' + model_data.id + ']'),
            item_html = LarakitManager.getRowHtml($list, model_data);
        if (!item_html) {
            return;
        }

        $item
            .replaceWith(item_html);
        $item = $list.children('.js-item[data-id=' + model_data.id + ']');
        $item
            .addClass('flash animated');
        setTimeout(function () {
            $item
                .removeClass('flash animated');
        }, 1000);
    },

    rowDelete: function ($list, id) {
        var $item = $list.children('.js-item[data-id=' + id + ']');
        $item
            .addClass("flipOutX animated");
        setTimeout(function () {
            $item
                .remove();
        }, 1000);
    },


    rows: function (model_key, models) {
        console.warn(model_key, models);
        var $items_list = $('.js-list[data-key=' + model_key + ']');
        $items_list.each(function () {
            var $list = $(this), js_filter = $list.attr('data-filter') || '',
                max = parseInt($list.attr('data-max-items')), model;
            for (i in models) {
                model = models[i];
                // console.info('.js-list[data-key=' + model_key + '][data-filter='+model.model.js_filter+']');
                // console.info($('.js-list[data-key=' + model_key + '][data-filter='+model.model.js_filter+']').length);
                //если такой группы вообще нет - перезагрузим страницу
                /*if(!$('.js-list[data-key=' + model_key + '][data-filter='+model.model.js_filter+']').length && js_filter){
                 window.location.reload();
                 return false;
                 }*/
                if (js_filter) {
                    var $lists = $('.js-list[data-key=' + model_key + ']'),
                        cnt = 0;
                    $lists.each(function () {
                        if($(this).attr('data-filter')){
                            cnt += LarakitManager.filter($(this).attr('data-filter'), model.model.js_filter);
                        }
                    });
                    if (!cnt) {
                        window.location.reload();
                        return false;
                    }
                }
                if (('undefined' != typeof (model.model)) && LarakitManager.filter(js_filter, model.model.js_filter)) {
                    if ($list.children('.js-item[data-id=' + model.model.id + ']').length) {
                        // console.info('update');
                        LarakitManager.rowUpdate($list, model);
                    } else {
                        // console.info('insert');
                        LarakitManager.rowInsert($list, model);
                    }
                } else {
                    // console.info('delete');
                    // console.info(model.id);
                    LarakitManager.rowDelete($list, model.id);
                }
            }
            if (max && !isNaN(max)) {
                $list.find('.js-item').slice(max).remove();
            }
            LarakitManager.sort($list);
        });
        $body.trigger('larakit.js');
    },

    filter: function (list_filters, model_filters) {
        if ('' == list_filters) return true;
        list_filters = list_filters.split(' ');
        for (list_filter in list_filters) {
            var list_filters_array = list_filters[list_filter].split('|');
            var cnt = 0;
            for (list_filter_array in list_filters_array) {
                if ($.inArray(list_filters_array[list_filter_array], model_filters) >= 0) {
                    cnt++;
                }
            }
            if (!cnt) return false;
        }
        return true;
    },

    //**********************************************************************
    // Сортировка списка после обновления
    //**********************************************************************
    sort: function ($list) {
        var sort_field = 'data-sort';
        $list
            .children('.js-item')
            .sort(function (a, b) {
                var first = sort_field ? $(a).attr(sort_field) : $(a).text(),
                    second = sort_field ? $(b).attr(sort_field) : $(b).text();
                return ((first > second) != false) ? 1 : -1;
            })
            .appendTo($list);
    }

};