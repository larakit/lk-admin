LarakitJs.initSelector('.js-crud-delete', function () {
    $(this).on('click', function () {
        if (confirm('Вы действительно хотите удалить?')) {
            var $list = LarakitManager.getList($(this)),
                id = LarakitManager.getId($(this));
            $.post($list.attr('data-url-base') + '/delete', {id: id}, function (response) {
                if ('success' == response.result) {
                    LarakitManager.rowDelete($list, id);
                }
                larakit_toastr(response);
            });
        }
    });
});

LarakitJs.initSelector('.js-crud-attach-delete', function () {
    $(this).on('click', function () {
        if (confirm('Вы действительно хотите удалить?')) {
            var $list = LarakitManager.getList($(this)),
                id = LarakitManager.getId($(this));
            $.post($list.attr('data-url-base') + '/attach-delete', {id: id}, function (response) {
                if ('success' == response.result) {
                    LarakitManager.rowDelete($list, id);
                }
                larakit_toastr(response);
            });
        }
    });
});

LarakitJs.initSelector('.js-crud-belongs-to-many-save', function () {
    $(this).on('click', function () {
        if (confirm('Вы действительно хотите удалить?')) {
            var $list = LarakitManager.getList($(this)),
                $item = LarakitManager.getItem($(this)),
                id = LarakitManager.getId($(this));
            $.post($list.attr('data-url-base') + '/belongs-to-many-save', {
                id: id,
                model_id: $item.attr('data-rel-id'),
                rel: $item.attr('data-rel')
            }, function (response) {
                if ('success' == response.result) {
                    LarakitManager.rowDelete($list, id);
                }
                larakit_toastr(response);
            });
        }
    });
});
LarakitJs.initSelector('.js-crud-belongs-to-many-delete', function () {
    $(this).on('click', function () {
        if (confirm('Вы действительно хотите удалить?')) {
            var $list = LarakitManager.getList($(this)),
                id = LarakitManager.getId($(this));
            $.post($list.attr('data-url-base') + '/belongs-to-many-delete', {id: id}, function (response) {
                if ('success' == response.result) {
                    LarakitManager.rowDelete($list, id);
                }
                larakit_toastr(response);
            });
        }
    });
});

LarakitJs.initSelector('.js-crud-attach-delete-all', function () {
    $(this).on('click', function () {
        var key = $(this).attr('data-key'),
            $list = $('.js-list[data-key=' + key + ']');
        if (confirm('Вы действительно хотите удалить ВСЕ вложения?')) {
            $list.children('.js-item').each(function () {
                var id = LarakitManager.getId($(this));
                $.post($list.attr('data-url-base') + '/attach-delete', {id: id}, function (response) {
                    if ('success' == response.result) {
                        LarakitManager.rowDelete($list, id);
                    }
                    larakit_toastr(response);
                });
            });
        }
    });
});

LarakitJs.initSelector('.js-crud-thumbs-delete-all', function () {
    $(this).on('click', function () {
        var key = $(this).attr('data-key'),
            $list = $('.js-list[data-key=' + key + ']'),
            type = $list.attr('data-type');
        if (confirm('Вы действительно хотите удалить ВСЕ миниатюры?')) {
            $list.children('.js-item').each(function () {
                var id = LarakitManager.getId($(this));
                $.post($list.attr('data-url-base') + '/thumbs-delete', {item_id: id, type: type}, function (response) {
                    if ('success' == response.result) {
                        LarakitManager.rowDelete($list, id);
                    }
                    larakit_toastr(response);
                });
            });
        }
    });
});

LarakitJs.initSelector('.js-crud-thumb-delete', function () {
    $(this).on('click', function () {
        if (confirm('Вы действительно хотите удалить?')) {
            var $list = LarakitManager.getList($(this)),
                key = LarakitManager.getListKey($(this)),
                type = $list.attr('data-type'),
                imageable_id = LarakitManager.getItem($(this)).attr('data-imageable-id'),
                id = LarakitManager.getId($(this)),
                url = $list.attr('data-url-base') + '/',
                data;

            if (imageable_id) {
                data = {id: imageable_id, type: type, item_id: id};
                url += 'thumbs-delete';
            } else {
                data = {id: id, type: type};
                url += 'thumb-delete';
            }
            $.post(url, data, function (response) {
                if ('success' == response.result) {
                    if (imageable_id) {
                        LarakitManager.rowDelete($list, id);
                    } else {
                        LarakitManager.rows(key, response.models);
                    }
                }
                larakit_toastr(response);
            });
        }
        return false;
    });
});

LarakitJs.initSelector('.js-crud-edit', function () {
    $(this).on('click', function () {
        var $list = LarakitManager.getList($(this)),
            id = LarakitManager.getId($(this)), url;
        $.ajax({
            url: $list.attr('data-url-base') + '/' + id + '/edit',
            dataType: "jsonp"
        });
    });
});

LarakitJs.initSelector('.js-crud-rel-belongs-to-many', function () {
    $(this).on('click', function () {
        var $list = LarakitManager.getList($(this)),
            id = LarakitManager.getId($(this)), url;
        $.ajax({
            url: $list.attr('data-url-base') + '/' + id + '/rel-belongs-to-many',
            dataType: "jsonp"
        });
    });
});

LarakitJs.initSelector('.js-crud-rel-select', function () {
    $(this).on('click', function () {
        var $item = $(this),
            url = $item.attr('data-url');
        $.ajax({
            url: url,
            data: {
                relation: $item.attr('data-relation')
            },
            dataType: "jsonp"
        });
    });
});

LarakitJs.initSelector('.js-crud-rel-has-many', function () {
    $(this).on('click', function () {
        var $list = LarakitManager.getList($(this)),
            id = LarakitManager.getId($(this)), url;
        $.ajax({
            url: $list.attr('data-url-base') + '/' + id + '/rel-has-many',
            dataType: "jsonp"
        });
    });
});

LarakitJs.initSelector('.js-crud-attach-edit', function () {
    $(this).on('click', function () {
        var $list = LarakitManager.getList($(this)),
            id = LarakitManager.getId($(this)), url;
        $.ajax({
            url: $list.attr('data-url-base') + '/attach-edit',
            dataType: "jsonp",
            data: {id: id}
        });
    });
});

LarakitJs.initSelector('.js-crud-thumbs-edit', function () {
    $(this).on('click', function () {
        var $list = LarakitManager.getList($(this)),
            id = LarakitManager.getId($(this)), url;
        $.ajax({
            url: $list.attr('data-url-base') + '/thumbs-edit',
            dataType: "jsonp",
            data: {id: id, type: $(this).attr('data-type')}
        });
        return false;
    });
});

LarakitJs.initSelector('.js-crud-thumb-crop', function () {
    $(this).on('click', function () {
        var $list = LarakitManager.getList($(this)),
            type = $list.attr('data-type'),
            id = LarakitManager.getId($(this)),
            imageable_id = LarakitManager.getItem($(this)).attr('data-imageable-id'),
            url, data;
        if (imageable_id) {
            data = {id: imageable_id, type: type, item_id: id};
        } else {
            data = {id: id, type: type};
        }
        $.ajax({
            url: $list.attr('data-url-base') + '/thumb-crop',
            dataType: "jsonp",
            data: data
        });
        return false;
    });
});

LarakitJs.initSelector('.js-crud-thumb-crop-size', function () {
    $(this).on('click', function () {
        var url_base = $(this).attr('data-url-base'),
            type = $(this).attr('data-type'),
            size = $(this).attr('data-size'),
            item_id = $(this).attr('data-item-id'),
            id = $(this).attr('data-id');
        $.ajax({
            url: url_base + '/thumb-crop-size',
            dataType: "jsonp",
            data: {id: id, type: type, size: size, item_id: item_id}
        });
        return false;
    });
});

LarakitJs.initSelector('.js-crud-thumb', function () {
    $(this).on('click', function () {
        var $list = LarakitManager.getList($(this)),
            id = LarakitManager.getId($(this));
        $.ajax({
            url: $list.attr('data-url-base') + '/' + id + '/thumb',
            dataType: "jsonp"
        });
    });
});

LarakitJs.initSelector('.js-crud-thumbs', function () {
    $(this).on('click', function () {
        var $list = LarakitManager.getList($(this)),
            id = LarakitManager.getId($(this));
        $.ajax({
            url: $list.attr('data-url-base') + '/' + id + '/thumbs',
            dataType: "jsonp"
        });
    });
});

LarakitJs.initSelector('.js-crud-attaches', function () {
    $(this).on('click', function () {
        var $list = LarakitManager.getList($(this)),
            id = LarakitManager.getId($(this));
        $.ajax({
            url: $list.attr('data-url-base') + '/' + id + '/attaches',
            dataType: "jsonp"
        });
    });
});

LarakitJs.initSelector('.js-crud-toggler', function () {
    $(this).on('click', function () {
        LarakitManager.getItem($(this)).toggleClass('collapsed-box box-solid');
    });
});

var LarakitUpload = {
    //
    thumb: function (response) {
        LarakitManager.rows($(this).attr('data-key'), response.models);
    },
    //
    thumbs: function (response) {
        LarakitManager.rows($(this).attr('data-key'), response.models);
    },
    //
    attach: function (response) {
        LarakitManager.rows($(this).attr('data-key'), response.models);
    }
};

LarakitJs.initSelector('.modal', function () {
    $(this).attr("tabindex", '1').removeAttr('tabindex');
});