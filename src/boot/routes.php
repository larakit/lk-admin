<?php
\Larakit\Route\Route::item('admin')
                    ->setBaseUrl('/admincp/')
                    ->addMiddleware(['web','admin'])
                    ->setController('AdminController')
                    ->setNamespace('Larakit\Http\Controllers')
                    ->put();

if(\Request::is('*admin*')) {
    \LaraPage::body()->addClass(env('larakit.adminlte.body_class', 'skin-blue sidebar-mini'));
}

define('ROUTE_ATTACH_DOWNLOAD', 'attach-download.attach');
\Larakit\Route\Route::item('attach-download')
                    ->setBaseUrl('/!/download/')
                    ->addSegment('{attach}')
                    ->setUses(function ($id) {
                        $model = \Larakit\Models\LarakitAttach::findOrFail((int)\Illuminate\Support\Arr::get(hashids_decode($id),0));
                        $url   = $model->getUrl();
                        if($url) {
                            return Response::download(public_path($url), $model->name . '.' . $model->ext);
                        }
                        throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Файл не найден!');
                    })
                    ->put();
