<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 20.07.16
 * Time: 14:41
 */

namespace Larakit\FormFilters;

use Larakit\CRUD\TraitEntity;
use Larakit\QuickForm\LaraForm;

abstract class FormFilter {
    
    use TraitEntity;
    
    protected $filters = [];
    /**
     * @var LaraForm
     */
    protected $form;
    protected $message_list_empty        = 'Ничего не найдено. Добавьте первую запись';
    protected $message_list_empty_filter = 'Ничего не найдено. Измените условия поиска';
    protected $model;
    protected $per_page                  = 20;
    protected $title                     = 'Фильтры списка';
    protected $url_base                  = '';
    
    function __construct() {
        $model_class    = static::classModel();
        $this->model    = $model_class::select();
        $this->url_base = \URL::current();
        $this->form     = new LaraForm('filter_' . static::getEntitySnake(), 'get');
    }
    
    static function getEntitySuffix() {
        return 'FormFilter';
    }
    
    static function toArray() {
        $model_class = static::classModel();
        /** @var FormFilter $formfilter */
        $formfilter = new static();
        $formfilter->init();
        if($model_class::getGroupColumn()) {
            $formfilter->model->orderBy($model_class::getGroupColumn());
        }
        $formfilter->model->sorted();
        $box  = $formfilter->form->putAlteBox($formfilter->title);
        $body = $box->putAlteBoxBody()->removeClass('box-primary');
        foreach($formfilter->filters as $filter) {
            /* @var $filter Filter */
            $filter->element($body);
            $filter->query($formfilter->model);
        }
        if($formfilter->form->isSubmitted()) {
            $box->addClass('box-solid box-success');
        } else {
            $box->addClass('box-default');
        }
        $footer = $box->putAlteBoxFooter();
        $footer->putSubmitTwbs('Применить')->addClass('btn-success btn-disabled col-lg-6');
        if($formfilter->form->isSubmitted()) {
            $footer->putButtonLinkTwbs($formfilter->url_base, 'Сбросить')->addClass('btn-default col-lg-6');
        }
        
        return [
            'formfilter' => [
                'params'              => $_GET,
                'url_base'            => $formfilter->url_base,
                'form_filter'         => $formfilter->form,
                'per_page'            => $formfilter->per_page,
                'group_column'        => $model_class::getGroupColumn(),
                'model_key'           => $model_class::getModelKey(),
                'group_column_value'  => $model_class::getGroupColumnValue(),
                'message_list_empty'  => $formfilter->form->isSubmitted() ? $formfilter->message_list_empty_filter : $formfilter->message_list_empty,
                'is_form_filter_send' => $formfilter->form->isSubmitted(),
                'models'              => $formfilter->model->paginate($formfilter->per_page)->appends($_GET),
            ],
        ];
    }
    
    abstract function init();
    
    protected function addFilter($filter) {
        $this->filters[] = $filter;
    }
    
}