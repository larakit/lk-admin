<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 20.07.16
 * Time: 19:42
 */

namespace Larakit\FormFilters;

class FilterEqual extends Filter {

    function element(\HTML_QuickForm2_Container $form) {
        $form->putTextTwbs($this->form_field)
            ->setLabel($this->label)
            ->setDesc($this->desc)
            ->setAppendClear();

    }

    function query($model) {
        if($this->value) {
            if($this->relation) {
                $model->whereHas($this->relation, function ($query) {
                    $db_field = (array)$this->db_field;
                    foreach($db_field as $_db_field) {
                        $query->orWhere($_db_field, '=', $this->value );
                    }
                });
            } else {
                $model->where(function ($query) {
                    $db_field = (array)$this->db_field;
                    foreach($db_field as $_db_field) {
                        $query->orWhere($_db_field, '=', $this->value );
                    }
                });
            }
        }
    }
}