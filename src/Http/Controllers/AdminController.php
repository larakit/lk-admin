<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 26.07.16
 * Time: 9:52
 */

namespace Larakit\Http\Controllers;

use Larakit\Controller;

class AdminController extends Controller {

    protected $layout = 'larakit::admin';

    function index() {
        return $this->response();
    }
}