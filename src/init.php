<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 26.07.16
 * Time: 10:15
 */
\Larakit\Boot::init_package('larakit/lk-admin');
\Larakit\Boot::register_boot(__DIR__ . '/boot');
\Larakit\Boot::register_migrations(__DIR__ . '/migrations');
\Larakit\Boot::register_view_path(__DIR__ . '/views', 'lk-admin');
\Larakit\Boot::register_command(\Larakit\Commands\Make::class);
\Larakit\Boot::register_command(\Larakit\Commands\MakeAccessor::class);
\Larakit\Boot::register_command(\Larakit\Commands\MakeAttach::class);
\Larakit\Boot::register_command(\Larakit\Commands\MakeBoot::class);
\Larakit\Boot::register_command(\Larakit\Commands\MakeController::class);
\Larakit\Boot::register_command(\Larakit\Commands\MakeFormFilter::class);
\Larakit\Boot::register_command(\Larakit\Commands\MakeModel::class);
\Larakit\Boot::register_command(\Larakit\Commands\MakeQuickForm::class);
\Larakit\Boot::register_command(\Larakit\Commands\MakeThumb::class);
\Larakit\Boot::register_command(\Larakit\Commands\MakeThumbs::class);
\Larakit\Boot::register_command(\Larakit\Commands\MakeView::class);

\Larakit\Boot::register_middleware_group('admin', \Larakit\Middlewares\AdminCheck::class,100500);
\Larakit\Boot::register_middleware_group('admin', \Larakit\Middlewares\AdminNavigation::class);