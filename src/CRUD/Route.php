<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 04.08.16
 * Time: 13:59
 */

namespace Larakit\CRUD;

class Route {

    /**
     * @param      $as
     * @param      $base_url
     * @param null $icon
     *
     * @return \Larakit\Route\Route
     */
    static function crud($as, $base_url, $icon = null, $middleware = null, $namespace = null) {
        //################################################################################
        // INDEX
        //################################################################################
        $r          = \Larakit\Route\Route::item($as)
                                          ->addMiddleware($middleware)
                                          ->setIcon($icon)
                                          ->setNamespace($namespace)
                                          ->setBaseUrl($base_url)
                                          ->setAction('crudIndex')
                                          ->put('get');
        $controller = $r->getController();
        //################################################################################
        // INDEX
        //################################################################################
        $r->addSegment('add')
          ->setController($controller)
          ->setIcon('fa fa-plus')
          ->setAction('crudAdd')
          ->put('get')
          ->setAction('crudAdd')
          ->put('post');
        //################################################################################
        // DELETE
        //################################################################################
        $r->popSegment()
          ->addSegment('delete')
          ->setController($controller)
          ->setAction('crudItemDelete')
          ->put('post');
        $r->clearSegments()
          ->addSegment('{id}');
        //################################################################################
        // ITEM EDIT
        //################################################################################
        $r
            ->addSegment('edit')
            ->setController($controller)
            ->setAction('crudItemEdit')
            ->put('get')
            ->setAction('crudItemEdit')
            ->put('post');

        return $r;
    }

    static function thumb(\Larakit\Route\Route $r) {
        $r->clearSegments();
        $controller = $r->getController();
        $r
            ->addSegment('thumb-delete')
            ->setController($controller)
            ->setAction('thumbDelete')
            ->put('post');
        $r
            ->popSegment()
            ->addSegment('thumb-crop')
            ->setController($controller)
            ->setAction('thumbCrop')
            ->put('get');
        $r
            ->popSegment()
            ->addSegment('thumb-crop-size')
            ->setController($controller)
            ->setAction('thumbCropSize')
            ->put('get')
            ->setAction('thumbCropSizeSave')
            ->put('post');
        $r->clearSegments()->addSegment('{id}');
        $r
            ->addSegment('thumb')
            ->setController($controller)
            ->setAction('thumb')
            ->put();
        $r
            ->popSegment()
            ->addSegment('thumb-upload')
            ->setController($controller)
            ->setAction('thumbUpload')
            ->put();
    }

    static function thumbs(\Larakit\Route\Route $r) {
        $r->clearSegments();
        $controller = $r->getController();
        $r
            ->popSegment()
            ->addSegment('thumbs-delete')
            ->setController($controller)
            ->setAction('thumbsDelete')
            ->put('post');
        $r
            ->popSegment()
            ->addSegment('thumbs-edit')
            ->setController($controller)
            ->setAction('thumbsEdit')
            ->put();
        $r->clearSegments()->addSegment('{id}');
        $r->addSegment('thumbs')
          ->setController($controller)
          ->setAction('thumbs')
          ->put();
        $r->popSegment()
          ->addSegment('thumbs-wysiwyg')
          ->setController($controller)
          ->setAction('thumbsWysiwygInsert')
          ->put();
        $r
            ->popSegment()
            ->addSegment('thumbs-upload')
            ->setController($controller)
            ->setAction('thumbsUpload')
            ->put();

    }

    static function relBelongsToMany(\Larakit\Route\Route $r) {
        $r->clearSegments()->addSegment('{id}');
        $controller = $r->getController();
        $r
            ->addSegment('rel-belongs-to-many')
            ->setController($controller)
            ->setAction('crudRelBelongsToMany')
            ->put();
        $r
            ->addSegment('rel-belongs-to-many-save')
            ->setController($controller)
            ->setAction('crudRelBelongsToManyDelete')
            ->put();
        $r
            ->popSegment()
            ->addSegment('rel-belongs-to-many-delete')
            ->setController($controller)
            ->setAction('crudRelBelongsToManyDelete')
            ->put();
        self::relSelect($r);
    }

    protected static function relSelect(\Larakit\Route\Route $r) {
        $r->clearSegments()->addSegment('{id}');
        $controller = $r->getController();
        $r
            ->addSegment('rel-select')
            ->setController($controller)
            ->setAction('crudRelSelect')
            ->put();
    }

    static function relHasMany(\Larakit\Route\Route $r) {
        $r->clearSegments()->addSegment('{id}');
        $controller = $r->getController();
        $r
            ->addSegment('rel-has-many')
            ->setController($controller)
            ->setAction('crudRelHasMany')
            ->put();
        self::relSelect($r);
    }

    static function attach(\Larakit\Route\Route $r) {
        $r->clearSegments();
        $controller = $r->getController();
        $r
            ->addSegment('attach-delete')
            ->setController($controller)
            ->setAction('attachDelete')
            ->put('post');
        $r
            ->popSegment()
            ->addSegment('attach-edit')
            ->setController($controller)
            ->setAction('attachEdit')
            ->put('get')
            ->setAction('attachEdit')
            ->put('post');
        $r->clearSegments()->addSegment('{id}');
        $r
            ->addSegment('attach-upload')
            ->setController($controller)
            ->setAction('attachUpload')
            ->put();

        $r->popSegment()
          ->addSegment('attaches')
          ->setController($controller)
          ->setAction('attaches')
          ->put();
    }
}