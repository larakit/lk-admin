<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 04.08.16
 * Time: 12:10
 */

namespace Larakit\CRUD;

use BootstrapDialog\BootstrapDialog;
use BootstrapDialog\BootstrapDialogButton;
use Illuminate\Support\Arr;
use Larakit\Helpers\HelperModel;

trait TraitControllerCRUDRelSelect {

    function crudRelSelect() {

        /** @var \Eloquent $model_name */
        $model_name    = static::classModel();
        $relation      = \Request::input('relation');
        $method        = HelperModel::getMethod($model_name, $relation);
        $model         = $model_name::with($relation)->findOrFail(\Request::route('id'));
        $related_class = $model->{$relation}()->getRelated();
        switch(Arr::get($method, 'relType')) {
            case 'belongsTo':
                $max = 1;
                break;
            default;
                $max = (int) Arr::get($method, 'tags.relSelectMax.0', 0);
                break;
        }

        if('POST' == \Request::method()) {
            $selected = \Request::input('selected');
            $selected = (array) $selected;
            $model->{$relation}()->sync($selected);
            $model    = $model_name::with($relation)->findOrFail(\Request::route('id'));
            $selector = '.js-belongs-to-many';
            $selector .= '[data-model=' . $model_name::getModelKey() . ']';
            $selector .= '[data-id=' . $model->id . ']';
            $selector .= '[data-relation=' . $relation . ']';
            $data = [
                'selector'  => $selector,
                '$selected' => $selected,
                'html'      => (string) view('larakit::!.crud.lists.rel_belongs_to_many', [
                    'rel_name' => $relation,
                    'model'    => $model,
                ]),
            ];

            return $this->crudSuccess('Связи сохранены!', $data);
        }

        $content = file_get_contents(base_path('vendor/larakit/lk-admin/src/javascripts/angular-rel-select.js'));
//        dd(
//            $related_class::all()->toArray(),
//            $model->{$relation}->keyBy('id')->toArray()
//        );
        $content = str_replace('"{items}"', json_encode($related_class::all()->toArray()), $content);
        $content = str_replace('"{selected}"', json_encode(array_keys($model->{$relation}->keyBy('id')->toArray())), $content);
        $content = str_replace('"{model}"', json_encode($model->toArray()), $content);
        $content = str_replace('"{relation}"', json_encode($relation), $content);
        $content = str_replace('"{max}"', $max, $content);
//dd($content);
//        dd($model_name,$_m->class, $method, $model->toArray());
        $form = view('larakit::!.crud.rel_select', [
            'method' => $method,
            'url'    => \URL::current(),
        ]);

        return $this->crudWrapFormDialogRelSelect(BootstrapDialog::factory()
            ->setTypePrimary()
            ->setSizeWide()
            ->setOnshown($content)
            ->setTitle('Выберите вариант(ы)')
            ->setMessage($form)
            ->addButton(
                BootstrapDialogButton::factory('Сохранить')
                    ->addClassBtnPrimary()
                    ->setActionFile(base_path('vendor/larakit/lk-admin/src/javascripts/crud-rel-selected.js'))
                    ->addClass('js-submit-rel-selected')
            )
            ->addButton(
                BootstrapDialogButton::factory()
            )
        )
            ->send();
    }

    function crudWrapFormDialogRelSelect(BootstrapDialog $dialog) {
        return $this->crudWrapFormDialog($dialog);
    }

}