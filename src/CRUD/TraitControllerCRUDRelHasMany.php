<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 04.08.16
 * Time: 12:10
 */

namespace Larakit\CRUD;

use BootstrapDialog\BootstrapDialog;
use BootstrapDialog\BootstrapDialogButton;
use Illuminate\Support\Arr;
use Larakit\Helpers\HelperModel;
use Larakit\ValidateException;

trait TraitControllerCRUDRelHasMany {

    function crudRelHasMany() {
        $model_name = static::classModel();

        $relations = HelperModel::getHasMany($model_name);
        $keys      = $this->crudRelHasManyRelations();
        if($keys) {
            $relations = Arr::only($relations, $keys);
        }
        /** @var \Eloquent $model_name */
        if(!count($relations)) {
            throw new ValidateException('Такой связи не существует [' . $model_name . ']');
        }
        $model = $model_name::with(array_keys($relations))->findOrFail(\Request::route('id'));
        $form  = view('larakit::!.crud.rel_has_many', [
            'url_base'  => route($this->routeBase()),
            'relations' => $relations,
            'model'     => $model,
        ]);

        return $this->crudWrapFormDialog(BootstrapDialog::factory()
            ->setTypePrimary()
            ->setSizeWide()
            ->setTitle('Зависимости типа "один ко многим"')
            ->setMessage($form)
            ->addButton(
                BootstrapDialogButton::factory()
            ))
            ->send();
    }

    function crudRelHasManyRelations() {
        return ['variant_videos'];
    }

}