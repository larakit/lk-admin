<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 04.08.16
 * Time: 12:10
 */

namespace Larakit\CRUD;

use BootstrapDialog\BootstrapDialog;
use BootstrapDialog\BootstrapDialogButton;
use Larakit\Helpers\HelperModel;
use Larakit\ValidateException;

trait TraitControllerCRUDRelBelongsToMany {

    function crudRelBelongsToManyDelete() {

    }

    static function isUseBelongsToMany(){
        return true;
    }

    function crudRelBelongsToMany() {
        /** @var \Eloquent $model_name */
        $model_name = static::classModel();
        $relations  = HelperModel::getBelongsToMany($model_name);
        if(!count($relations)) {
            throw new ValidateException('Такой связи не существует [' . $model_name . ']');
        }
        $model = $model_name::with(array_keys($relations))->findOrFail(\Request::route('id'));
        $form  = view('larakit::!.crud.rel_belongs_to_many', [
            'url_base'  => route($this->routeBase()),
            'relations' => $relations,
            'model'     => $model,
        ]);

        return $this->crudWrapFormDialog(BootstrapDialog::factory()
            ->setTypePrimary()
            ->setSizeWide()
            ->setTitle('Зависимости типа "многие ко многим"')
            ->setMessage($form)
            ->addButton(
                BootstrapDialogButton::factory()
            ))
            ->send();
    }

}