<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 04.08.16
 * Time: 12:10
 */

namespace Larakit\CRUD;

use BootstrapDialog\BootstrapDialog;
use BootstrapDialog\BootstrapDialogButton;
use Illuminate\Database\Eloquent\Model;
use Larakit\Accessors\Accessor;
use Larakit\ACL\Acl;
use Larakit\ValidateException;

trait TraitControllerCRUD {

    function __construct() {
        \LaraPage::addBreadCrumb('admin')
                 ->addBreadCrumb($this->routeBase());
        $this->base_url = \URL::current();
    }

    function crudIndex() {
        $form_filter = static::classFormFilter();

        return $this->response(
            $form_filter::toArray() +
            [
                'url_base' => \URL::current(),
                'key'      => md5(static::classModel()),
            ]);
    }

    function crudItemPostData() {

    }
    function crudItemEditSave($model, $form) {
        $model->fill($form->getValue());
        $model->save();
        return $model;
    }

    function crudItemEdit() {
        /** @var \Eloquent $model_name */
        $model_name = static::classModel();
        $model      = $model_name::findOrFail(\Request::route('id'));
        $acl        = Acl::factory($model, $model->id);
        $reason     = $acl->reason('edit');
        if($reason) {
            throw new ValidateException($reason);
        }
        $form_class = static::classForm();
        $form       = new $form_class('edit');
//        dd($model->toForm());
        $form->initValues($model->toForm());
        if(\Request::isMethod('post')) {
            if($form->validate()) {
                $model = $this->crudItemEditSave($model, $form);
                return $this->crudSuccess('Запись успешно сохранена!', ['models' => $this->crudModels($model)]);
            } else {
                return $this->crudErrors('Ошибки при заполнении формы', $form);
            }
        }

        return $this->crudWrapFormDialogEdit(BootstrapDialog::factory()
                                                            ->setTypePrimary()
                                                            ->setTitle('Редактирование')
                                                            ->setMessage($form)
                                                            ->setClosable(0)
                                                            ->setDataItem('key', md5(static::classModel()))
                                                            ->addButton(
                                                                BootstrapDialogButton::factory('Сохранить')
                                                                                     ->addClassBtnPrimary()
                                                                                     ->setActionFormSubmit()
                                                            )->addButton(
                BootstrapDialogButton::factory()
            ))
                    ->send();
    }

    function crudSuccess($message, array $data = []) {
        return array_merge($data, [
            'result'  => 'success',
            'message' => $message,
        ]);
    }

    /**
     * @param Model $model
     *
     * @return array
     */
    function crudModels(Model $model) {
        $accessor = Accessor::factory($model);

        return [
            [
                'model' => $model->toArray(),
                'rows'  => $accessor->rows(),
                'id'    => $model->id,
            ],
        ];
    }

    function crudErrors($message, $body = null) {
        $ret = [
            'result'  => 'error',
            'message' => $message.': <br>' . implode('<br>', $body->laravel_errors),
        ];
        if($body) {
            $ret['body'] = (string) $body;
        }

        return $ret;
    }

    function crudWrapFormDialogEdit(BootstrapDialog $dialog) {
        return $this->crudWrapFormDialog($dialog);
    }

    function crudWrapFormDialog(BootstrapDialog $dialog) {
        return $dialog;
    }

    function crudRowTemplate() {
        return 'larakit::!.crud._.row';
    }

    function crudAddSave($form) {
        $model_name = static::classModel();
        return $model_name::create($form->getValue());
    }

    function crudAdd() {
        $form_class = static::classForm();
        $form       = new $form_class('add');
        if(\Request::isMethod('post')) {
            if($form->validate()) {
                $model = $this->crudAddSave($form);

                return $this->crudSuccess('Запись успешно сохранена!', ['models' => $this->crudModels($model)]);
            } else {
                return $this->crudErrors('Ошибки при заполнении формы', $form);
            }
        }

        return $this->crudWrapFormDialogAdd(BootstrapDialog::factory()
                                                           ->setTypePrimary()
                                                           ->setTitle('Добавление')
                                                           ->setMessage($form)
                                                           ->setClosable(0)
                                                           ->setDataItem('key', md5(static::classModel()))
                                                           ->addButton(
                                                               BootstrapDialogButton::factory('Добавить')
                                                                                    ->addClassBtnPrimary()
                                                                                    ->setActionFormSubmit()
                                                           )->addButton(
                BootstrapDialogButton::factory()
            )
        )->send();
    }

    function crudWrapFormDialogAdd(BootstrapDialog $dialog) {
        return $this->crudWrapFormDialog($dialog);
    }

    function crudItemDelete() {
        $model_name = static::classModel();
        $id         = \Request::input('id');
        $model      = $model_name::findOrFail($id);
        if($model->delete()) {
            return $this->crudSuccess('Запись успешно удалена!');
        } else {
            return $this->crudErrors('Эту запись удалить нельзя!');
        }
    }

}