<?php
namespace Larakit\Models;

use App\TraitThumb;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Larakit\CRUD\TraitModelRows;

/**
 * Larakit\Models\LarakitAttach
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $order
 * @property string $type
 * @property integer $attachable_id
 * @property string $attachable_type
 * @property string $ext
 * @property string $size
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $attachable
 * @method static \Illuminate\Database\Query\Builder|\Larakit\Models\LarakitAttach whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Larakit\Models\LarakitAttach whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\Larakit\Models\LarakitAttach whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Larakit\Models\LarakitAttach whereAttachableId($value)
 * @method static \Illuminate\Database\Query\Builder|\Larakit\Models\LarakitAttach whereAttachableType($value)
 * @method static \Illuminate\Database\Query\Builder|\Larakit\Models\LarakitAttach whereExt($value)
 * @method static \Illuminate\Database\Query\Builder|\Larakit\Models\LarakitAttach whereSize($value)
 * @method static \Illuminate\Database\Query\Builder|\Larakit\Models\LarakitAttach whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Larakit\Models\LarakitAttach whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Larakit\Models\LarakitAttach whereUpdatedAt($value)
 */
class LarakitAttach extends Model {

    use TraitModelRows;
    protected $fillable = [
        'type',
        'attachable_id',
        'attachable_type',
        'order',
        'ext',
        'size',
        'name',
    ];
    protected $hidden   = [
        'order',
    ];
    protected $table    = 'larakit__attaches';

    function larakitRows() {
        return [
            'attach' => '',
        ];
    }

    public function attachable() {
        return $this->morphTo();
    }

    function downloadUrl() {
        return route(ROUTE_ATTACH_DOWNLOAD, hashids_encode($this->id));
    }

    function setNameAttribute($v) {
        $this->attributes['name'] = trim($v);
    }

    function setExtAttribute($v) {
        $this->attributes['ext'] = Str::lower($v);
    }

    function getUrl() {
        try {
            $class        = $this->attachable_type;
            $attach_class = $class::classAttach($this->type);

            return $attach_class::factory($this->attachable_id, $this->id)->getUrl($this->ext);
        }
        catch(\Exception $e) {
            return null;
        }
    }

}

LarakitAttach::deleted(function ($model) {
    $file = public_path($model->getUrl());
    \File::delete($file);
});