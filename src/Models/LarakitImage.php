<?php
namespace Larakit\Models;

use App\TraitThumb;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Larakit\CRUD\TraitEntity;
use Larakit\CRUD\TraitModelRows;
use Larakit\Thumb\Thumb;
use Larakit\Thumb\TraitModelThumb;

/**
 * App\Model\Adv
 *
 * @property integer                                                          $id
 * @property string                                                           $name
 * @property string                                                           $format
 * @property string                                                           $desc
 * @property integer                                                          $adv_type_id
 * @property integer                                                          $order
 * @property string                                                           $deleted_at
 * @property \Carbon\Carbon                                                   $created_at
 * @property \Carbon\Carbon                                                   $updated_at
 * @property integer                                                          $type_id
 * @mixin \Eloquent
 * @property-read mixed                                                       $thumb_sizes
 * @property-read mixed                                                       $thumbs
 * @property string $type
 * @property integer $imageable_id
 * @property string $imageable_type
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $imageable
 * @method static \Illuminate\Database\Query\Builder|\Larakit\Models\LarakitImage whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Larakit\Models\LarakitImage whereImageableId($value)
 * @method static \Illuminate\Database\Query\Builder|\Larakit\Models\LarakitImage whereImageableType($value)
 */
class LarakitImage extends Model {
    use TraitModelRows;
    use TraitEntity;
    use TraitModelThumb;
    protected $table    = 'larakit__images';
    protected $hidden   = [
        'order',
    ];
    protected $fillable = [
        'name',
        'desc',
        'type',
        'imageable_id',
        'imageable_type',
        'order',
    ];

    public function imageable() {
        return $this->morphTo();
    }

    function larakitRows() {
        return [
            'thumb' => '',
            'thumbs' => '',
        ];
    }

    function getUrl($size=null) {
        try {
            $class        = $this->imageable_type;
            $attach_class = $class::classThumb($this->type);

            return $attach_class::factory($this->imageable_id, $this->id)->getUrl($size);
        }
        catch(\Exception $e) {
            return null;
        }
    }

    /**
     * @param $type
     *
     * @return Thumb
     */
    function classModelThumb($type) {
        $model_class = $this->imageable_type;

        return $model_class::getVendorStudly() . '\Thumbs\\' . $model_class::getEntityStudly() . Str::studly($type) . 'Thumb';
    }

    function thumb($type = 'default', $item_id = null) {
        $class = $this->classModelThumb($type);

        return $class::factory($this->imageable_id, $this->id);
    }


}