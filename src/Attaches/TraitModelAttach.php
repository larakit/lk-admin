<?php
namespace Larakit\Attaches;

use Larakit\Models\LarakitAttach;

/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 31.07.16
 * Time: 16:00
 * @mixin \Larakit\CRUD\TraitEntity
 * @mixin \Eloquent
 */
trait TraitModelAttach {

    public function larakitAttaches() {
        return $this->morphMany(LarakitAttach::class, 'attachable');
    }
}