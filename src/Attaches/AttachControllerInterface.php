<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 04.08.16
 * Time: 9:51
 */

namespace Larakit\Attaches;

interface AttachControllerInterface {

//    function attachTypes() {
//        return [
//            'docs',
//            'group',
//        ];
//    }
//
//    function routeBase() {
//        return 'admin.recommend_group';
//    }

    /**
     * @return array
     */

    function attachTypes();

    /**
     * @return string
     */
    function routeBase();

}