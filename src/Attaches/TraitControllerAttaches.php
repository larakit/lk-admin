<?php
namespace Larakit\Attaches;

use BootstrapDialog\BootstrapDialogButton;
use Illuminate\Http\UploadedFile;
use Larakit\CRUD\TraitEntity;
use Larakit\Models\LarakitAttach;
use Larakit\QuickForm\LaraForm;

/**
 * Class TraitControllerAttaches
 * @package Larakit\Attaches
 * @mixin TraitEntity
 */
trait TraitControllerAttaches {

    function attachEdit() {
        $id           = \Request::input('id');
        $attach_model = LarakitAttach::findOrFail($id);
        $form         = new LaraForm('attach');
        $form->addHidden('id');
        $form->putTextTwbs('name')->setLabel('Название файла')
            ->setAppend('.' . $attach_model->ext)
            ->setDesc('Рекомендуется писать на русском языке, при скачивании будет сделан транслит')
            ->ruleRequired();
        $form->initValues($attach_model->toArray());

        if(\Request::isMethod('post')) {
            if($form->validate()) {
                $attach_model->name = \Request::input('name');
                $attach_model->save();

                return [
                    'result'  => 'success',
                    'message' => 'Запись успешно сохранена',
                    'models'  => [
                        [
                            'model' => $attach_model->toArray(),
                            'rows'  => [
                                $this->attachRowType() => (string) view('larakit::!.crud.rows.attach.' . $this->attachRowType(), [
                                    'model' => $attach_model,
                                ]),
                            ],
                            'id'    => $attach_model->id,
                        ],
                    ],

                ];
            } else {
                return [
                    'result'  => 'error',
                    'message' => 'Все пропало!!!',
                    'body'    => $form . '',
                ];
            }
        }
        $model_class = static::classModel();

        return \BootstrapDialog\BootstrapDialog::factory()
            ->setTypePrimary()
            ->setTitle('Управление вложениями')
            ->setNl2br(false)
            ->setMessage($form)
            ->setClosable(true)
            ->setDataItem('id', $id)
            ->setDataItem('key', md5($model_class . $attach_model->type))
            ->addButton(
                BootstrapDialogButton::factory('Сохранить')
                    ->setActionFile(base_path('vendor/larakit/lk-admin/src/javascripts/attach.js'))
            )
            ->send();
    }

    function attachDelete() {
        $id           = \Request::input('id');
        $attach_model = LarakitAttach::findOrFail($id);
        $attach_model->delete();

        return [
            'result'  => 'success',
            'message' => 'Файл успешно удален!',
            'id'      => $id,
        ];
    }

    function attachRowType() {
        return 'default';
    }

    function attachUpload() {
        $model_class = static::classModel();
        $type        = \Request::input('type');
        $model_id    = \Route::input('id');
        $model       = $model_class::findOrFail($model_id);
        $class       = static::classAttach($type);
        $f           = \Request::file('file');
        /* @var $f UploadedFile */
        $model_attach = LarakitAttach::create([
            'type' => $type,
            'ext'  => $f->getClientOriginalExtension(),
            'size' => $f->getClientSize(),
            'name' => mb_substr($f->getClientOriginalName(), 0, -1 - mb_strlen($f->getClientOriginalExtension())),
        ]);
        /* @var $attach \Larakit\Attaches\Attach */
        $attach = $class::factory($model_id, $model_attach->id);
        $attach->processing($f);
        $model_attach->attachable()->associate($model);
        $model_attach->save();

        return [
            'result'  => 'success',
            'message' => 'Файл  "' . $f->getClientOriginalName() . '" успешно загружен!',
            'models'  => [
                [
                    'model' => $model_attach->toArray(),
                    'rows'  => [
                        $this->attachRowType() => (string) view('larakit::!.crud.rows.attach.' . $this->attachRowType(), [
                            'model' => $model_attach,
                        ]),
                    ],
                    'id'    => $model_attach->id,
                ],
            ],
        ];
    }

    function attaches() {
        $content     = [];
        $model_class = static::classModel();
        $model_id    = \Route::input('id');
        $model       = $model_class::findOrFail($model_id);
        foreach($this->attachTypes() as $type) {
            $class     = static::classAttach($type);
            $content[] = view('larakit::!.crud.lists.attach.' . $this->attachRowType(), [
                'url_upload' => route($this->routeBase() . '.id.attach-upload', \Route::input('id')),
                'url_base'   => route($this->routeBase()),
                'row_type'   => $this->attachRowType(),
                'type'       => $type,
                'name'       => $class::getName(),
                'key'        => md5($model_class . $type),
                'attaches'   => $model->larakitAttaches()->where('type', '=', $type)->get(),
            ]);
        }

        return \BootstrapDialog\BootstrapDialog::factory()
            ->setTypePrimary()
            ->setTitle('Управление вложениями')
            ->setSizeWide()
            ->setNl2br(false)
            ->setMessage(implode('', $content))
            ->setClosable(true)
            ->addButton(
                BootstrapDialogButton::factory('Закрыть')
            )
            ->send();

    }

}