<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 27.07.16
 * Time: 8:32
 */

namespace Larakit\Attaches;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

abstract class Attach {

    /**
     * Идентификатор файла списка
     * @var int|null
     */
    protected $item_id = null;
    /**
     * Идентификатор модели для которой делается аттач либо аттач списка
     * @var int
     */
    protected $model_id = null;

    function __construct($model_id, $item_id) {
        $this->model_id = (int) $model_id;
        $this->item_id  = (int) $item_id;
    }

    static function getName() {
        return 'Вложения';
    }

    static function factory($model_id, $item_id) {
        return new static($model_id, $item_id);
    }

    /**
     *     function getPrefix() {
     *          return 'logo/sizename.ru';
     *     }
     * @return string
     */
    abstract function getPrefix();

    /**
     * Формирование ссылки на загруженный файл с проверкой наличия на диске
     *
     * @return mixed|null|string
     */
    function getUrl($ext) {
        $url  = $this->makeUrl($ext);
        $file = public_path($url);
        if(file_exists($file)) {
            return $url;
        }

        return null;
    }

    /**
     * Формирование ссылки на загруженный файл
     *
     * @param null $size
     *
     * # /!/attach/<getPrefix()>/3/2/123/<hashids_model>.<ext>
     * # /!/attach/<getPrefix()>/3/2/123/<size>.<ext>
     * # /!/attach/<getPrefix()>/3/2/123/g-<id_larakit_image>.<ext>
     * # /!/attach/<getPrefix()>/3/2/123/g-<id_larakit_image>-<size>.<ext>
     *
     * @return string
     */
    function makeUrl($ext) {
        $prefix   = [];
        $prefix[] = '!';
        $prefix[] = 'attach';
        $prefix[] = trim($this->getPrefix(), '/');
        $prefix[] = mb_substr($this->model_id, -1);
        $prefix[] = mb_substr($this->model_id, -2, 1);
        $prefix[] = $this->model_id;
        $link     = '/' . implode('/', $prefix) . '/';
        $link .= 'glk-' . hashids_encode($this->item_id);
        $link .= '.' . $ext;

        return $link;
    }

    function getMimes() {
        return [];
    }

    //массовое обновление всех превьюшек
    function processing($source) {
        if($source instanceof UploadedFile) {
            $ext    = $source->getClientOriginalExtension();
            $mime   = $source->getClientMimeType();
            $source = $source->path();

        } else {
            $ext  = \File::extension($source);
            $mime = \File::mimeType($source);
        }
        $ext = Str::lower($ext);
        if($this->getMimes()) {
            if(!in_array($mime, $this->getMimes())) {
                throw new \Exception('Запрещенный тип файла');
            }
        }
        $file = public_path($this->makeUrl($ext));
        $dir  = dirname($file);
        if(!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        copy($source, $file);
//        Event::notify('THUMB',
//            [
//                'entity' => $this->entity,
//                'vendor' => $this->vendor,
//                'name'   => $this->name,
//                'size'   => $size,
//                'w'      => $img->width(),
//                'h'      => $img->height(),
//                'file'   => $file,
//                'id'     => $this->id,
//            ]);

    }

}