<?php
namespace Larakit\Widgets;

/**
 * Class WidgetNavbarMenu
 *
 * @static  WidgetNavbarMenu instance($name = 'default')
 * @package Larakit\Widget;
 */
class WidgetNavbarMenu extends Widget {
    
    static protected $items = [];
    
    static function addItem($item) {
        static::$items[] = $item;
    }
    
    function toHtml() {
        $ret = [];
        foreach(static::$items as $tpl){
            $ret[] = view($tpl);
        }
        return implode('', $ret);
    }
    
}