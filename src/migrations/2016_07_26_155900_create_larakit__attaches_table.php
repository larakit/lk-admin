<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLarakitAttachesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('larakit__attaches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order')->default(0);
            $table->string('type');
            $table->integer('attachable_id');
            $table->string('attachable_type');
            $table->string('ext');
            $table->string('size');
            $table->string('name');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('larakit__attaches');
    }
}
