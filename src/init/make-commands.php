<?php
//регистрируем команды
Larakit\Boot::register_command(\Larakit\Commands\MakeQuickForm::class);
Larakit\Boot::register_command(\Larakit\Commands\MakeFormFilter::class);
Larakit\Boot::register_command(\Larakit\Commands\MakeThumb::class);
Larakit\Boot::register_command(\Larakit\Commands\MakeThumbs::class);
Larakit\Boot::register_command(\Larakit\Commands\MakeAttach::class);
Larakit\Boot::register_command(\Larakit\Commands\MakeController::class);
Larakit\Boot::register_command(\Larakit\Commands\MakeBoot::class);
Larakit\Boot::register_command(\Larakit\Commands\Make::class);
Larakit\Boot::register_command(\Larakit\Commands\MakeModel::class);
Larakit\Boot::register_command(\Larakit\Commands\MakeView::class);
