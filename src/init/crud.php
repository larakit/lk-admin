<?php

\Larakit\Twig::register_function('crud_rows', function ($formfilter, $row_type, $is_group = false) {
    return view('lk-admin::!.crud.lists._', compact('formfilter', 'row_type', 'is_group'));
});

\Larakit\Twig::register_function('crud_row', function ($model, $name = 'admin', $params = []) {
    return \Larakit\Accessors\Accessor::factory($model)->row($name, $params);
});
