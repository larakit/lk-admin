<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 26.07.16
 * Time: 10:15
 */
\Larakit\Widgets\ManagerWidget::register(\Larakit\Widgets\WidgetSidebarMenu::class, __DIR__ . '/../views');
\Larakit\Widgets\ManagerWidget::register(\Larakit\Widgets\WidgetNavbarMenu::class, __DIR__ . '/../views');
\Larakit\Widgets\ManagerWidget::register(\Larakit\Widgets\WidgetContentHeader::class, __DIR__ . '/../views');

\Larakit\QuickForm\Register::register('alte_box', 'qf_alte_box', __DIR__ . '/../views');
\Larakit\QuickForm\Register::register('alte_box_body', 'qf_alte_box_body', __DIR__ . '/../views');
\Larakit\QuickForm\Register::register('alte_box_footer', 'qf_alte_box_footer', __DIR__ . '/../views');

\Larakit\QuickForm\Register::container(\Larakit\QuickForm\ElementAlteBoxFooter::class);
\Larakit\QuickForm\Register::container(\Larakit\QuickForm\ElementAlteBox::class);
\Larakit\QuickForm\Register::container(\Larakit\QuickForm\ElementAlteBoxBody::class);

\Larakit\Twig::register_function('adminlte__box_wrapper_class', function ($id) {
    return 'ng-class="{\'collapsed-box\':!opened_' . $id . ', \'box-solid\':!opened_' . $id . ' }"';
});
\Larakit\Twig::register_function('adminlte__box_overlay', function ($id) {
    return '<div class="overlay hide"><i class="fa fa-refresh fa-spin"></i></div>';
});
\Larakit\Twig::register_function('adminlte__box_toggler', function ($id, $title = null) {
    if($title) {
        return '<span class="pointer js-crud-toggler">' . $title . '</span>';
    } else {
        return '<button class="btn btn-box-tool" type="button" ng-click="opened_' . $id . '=!opened_' . $id . '"><i class="fa " ng-class="{\'fa-plus\':!opened_' . $id . ', \'fa-minus\':opened_' . $id . '}"></i></button>';
    }
});

\Larakit\Twig::register_function('request_is', function () {
    $args = func_get_args();
    $args = array_map(function ($item) {
        return '*' . trim($item, '/') . '*';
    }, $args);
    
    return call_user_func_array(['Request', 'is'], $args);
});

\Larakit\Twig::register_function('icon_by_route', function ($route_name) {
    return \Larakit\Route\Route::routeIcons($route_name);
});

\Larakit\SEO::title('home', 'Главная страница');
\Larakit\SEO::title('admin', 'Админка');
\Larakit\StaticFiles\Manager::package('larakit/lk-admin')
    ->setSourceDir('public')
    ->usePackage('pear/html_quickform2', ['hierselect', 'repeat',])
    ->usePackage('larakit/sf-larakit-js')
    ->jsPackage('js/filter-daterange.js')
    ->jsPackage('js/qf-password-twbs.js')
//    ->jsPackage('js/lk-quickform.js')
    ->jsPackage('js/manager.js')
    ->jsPackage('js/crud.js')
    ->jsPackage('js/toCamelCase.js')
    ->cssPackage('css/AdminLTE.min.css')
    ->cssPackage('css/_all-skins.css')
    ->cssPackage('css/overlay.css')
    ->cssPackage('css/thumbs.css')
    ->cssPackage('css/filter-daterange.css')
//    ->cssPackage('css/lk-quickform.css')
    ->cssPackage('css/common.css');
