<?php

namespace Larakit\Middlewares;

use Closure;

class AdminNavigation {
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $dir = app_path('navigations');
        if(file_exists($dir)) {
            $files = rglob('*', 0, $dir);
            foreach($files as $file) {
                if(!is_dir($file)){
                    include $file;
                }
            }
        }
        
        return $next($request);
    }
}
