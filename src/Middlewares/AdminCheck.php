<?php

namespace Larakit\Middlewares;

use Closure;
use Larakit\TelegramBot;

class AdminCheck {
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if(!(bool) env('skip_auth')) {
            if(!\App\Me\Me::isAdmin()) {
                if(\App\Me\Me::id()) {
                    TelegramBot::add('Попытка входа в админку: ' . \App\Me\Me::fullName());
                    TelegramBot::send();
                }
                
                return redirect('/?no_admin');
            }
        }
        
        return $next($request);
    }
}
