<?php
namespace Larakit\Thumb;

use BootstrapDialog\BootstrapDialogButton;
use Larakit\Accessors\Accessor;
use Larakit\Models\LarakitImage;
use Larakit\ValidateException;

/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 31.07.16
 * Time: 16:00
 *
 * @mixin \Larakit\CRUD\TraitEntity
 * @mixin \Eloquent
 * @mixin ThumbControllerInterface
 */
trait TraitControllerThumb {
    
    function thumbCropSizeSave() {
        $id            = (int) \Request::input('id');
        $item_id       = (int) \Request::input('item_id');
        $size          = \Request::input('size');
        $type          = \Request::input('type');
        $x             = (int) \Request::input('cropper.x');
        $y             = (int) \Request::input('cropper.y');
        $crop_w        = (int) \Request::input('cropper.width');
        $crop_rotate   = (int) \Request::input('cropper.rotate');
        $crop_h        = (int) \Request::input('cropper.height');
        $thumb_class   = static::classThumb($type);
        $thumb         = $thumb_class::factory($id, $item_id);
        $file_original = public_path($thumb->getUrl());
        $crop          = \Image::make($file_original);
        if($crop_rotate) {
            $crop->rotate(0 - $crop_rotate, $thumb->getBg());
        }
        $crop_x_left   = ($x < 0) ? 0 : $x;
        $crop_y_top    = ($y < 0) ? 0 : $y;
        $crop_x_right  = min($crop->width(), $crop_w + $x);
        $crop_y_bottom = min($crop->height(), $crop_h + $y);
        $thumb_w       = $crop_x_right - $crop_x_left;
        $thumb_h       = $crop_y_bottom - $crop_y_top;
        $crop->crop($thumb_w, $thumb_h, $crop_x_left, $crop_y_top);
        
        $bg       = \Image::canvas($crop_w, $crop_h, $thumb->getBg());
        $offset_x = ($x < 0) ? abs($x) : 0;
        $offset_y = ($y < 0) ? abs($y) : 0;
        $bg->insert($crop, 'top-left', $offset_x, $offset_y);
        $thumb->processingSize($bg, $size);
        //$('  img')
        $data = [
            'result'    => 'success',
            'url'       => $thumb->getUrl($size) . '?' . microtime(true),
            'thumb_key' => $thumb->getKey($size),
            'message'   => 'Миниатюра успешно создана',
        ];
        
        return $data;
        
    }
    
    function thumbCropSize() {
        try {
            $model_class = static::classModel();
            $model_id    = \Request::input('id');
            $type        = \Request::input('type');
            $size        = \Request::input('size');
            $item_id     = \Request::input('item_id');
            $class       = static::classThumb($type);
            $thumb       = $class::factory($model_id, $item_id);
            if(!$thumb->getUrl()) {
                throw  new ValidateException('Картинка отсутствует');
            }
            $size_img = $thumb->getSize($size);
            $content  = view('larakit::!.crud.thumb_type_size', [
                'thumbs'   => $thumb->toArray(),
                'id'       => $model_id,
                'item_id'  => $item_id,
                'img'      => $thumb->getUrl(),
                'ratio'    => $size_img->getH() ? $size_img->getW() / $size_img->getH() : null,
                'config_w' => $size_img->getW(),
                'config_h' => $size_img->getH(),
                'type'     => $type,
                'url_base' => route($this->routeBase()),
            ]);
            
            return \BootstrapDialog\BootstrapDialog::factory()
                ->setTypePrimary()
                ->setTitle('Выберите миниатюру для подрезки')
                ->setSizeWide()
                ->setNl2br(false)
                ->setMessage($content)
                ->setDataItem('id', $model_id)
                ->setDataItem('size', $size)
                ->setDataItem('type', $type)
                ->setDataItem('item_id', $item_id)
                ->setDataItem('key', md5($model_class . $type))
                ->setDataItem('url', route($this->routeBase() . '.thumb-crop-size'))
                ->setClosable(true)
                ->addButton(
                    BootstrapDialogButton::factory()
                )
                ->addButton(
                    BootstrapDialogButton::factory('Сохранить')
                        ->addClassBtnPrimary()
                        ->setActionFile(base_path('vendor/larakit/lk-admin/src/javascripts/thumb-crop.js'))
                )
                ->send();
        } catch(\Exception $e) {
            return 'toastr.error("Миниатюра не требует подрезки, причина: "+' . json_encode($e->getMessage()) . ');';
        }
    }
    
    function thumbCrop() {
        try {
            $model_id = \Request::input('id');
            $item_id  = \Request::input('item_id');
            $type     = \Request::input('type');
            $class    = static::classThumb($type);
            $thumb    = $class::factory($model_id, $item_id);
            if(!$thumb->getUrl()) {
                throw  new ValidateException('Картинка отсутствует');
            }
            $content = view('larakit::!.crud.thumb_type', [
                'thumbs'   => $thumb->toArray(),
                'id'       => $model_id,
                'type'     => $type,
                'item_id'  => $item_id,
                'url_base' => route($this->routeBase()),
            ]);
            
            return \BootstrapDialog\BootstrapDialog::factory()
                ->setTypePrimary()
                ->setTitle('Выберите миниатюру для подрезки')
                ->setSizeWide()
                ->setNl2br(false)
                ->setMessage($content)
                ->setDataItem('key', $thumb->getKey('_'))
                ->setDataItem('item_id', $item_id)
                ->setClosable(true)
                ->addButton(
                    BootstrapDialogButton::factory()
                )
                ->send();
        } catch(\Exception $e) {
            return 'toastr.error("Миниатюра не требует подрезки, причина: "+' . json_encode($e->getMessage()) . ');';
        }
        
    }
    
    function thumbUpload() {
        $model_class = static::classModel();
        $type        = \Request::get('type');
        $model_id    = \Request::route('id');
        $model       = $model_class::findOrFail($model_id);
        $class       = static::classThumb($type);
        $f           = \Request::file('file');
        $thumb       = $class::factory($model_id);
        try {
            $thumb->processing($f);
        } catch(\Exception $e) {
            throw  new ValidateException('Ошибка при загрузке файла, проверьте тип загружаемого файла, должен быть изображением' . $e->getMessage());
        }
        
        $ret = [
            'result'  => 'success',
            'message' => 'Файл  "' . $f->getClientOriginalName() . '" успешно загружен!',
            'models'  => $this->thumbModels($model, $type),
        ];
        
        return $ret;
    }
    
    function thumbModels($model, $type) {
        $accessor = Accessor::factory($model);
        
        return [
            [
                'model' => $model->toArray(),
                'rows'  => $accessor->rows([
                    'thumb_type' => $type,
                    'url_base'   => route($this->routeBase()),
                ]),
                'id'    => $model->id,
            ],
        ];
    }
    
    function thumbRowType() {
        return 'thumb';
    }
    
    function thumbDelete() {
        $model_class = static::classModel();
        $type        = \Request::input('type');
        $model_id    = \Request::input('id');
        $item_id     = \Request::input('item_id');
        $model       = $model_class::findOrFail($model_id);
        if($item_id) {
            LarakitImage::where('id', '=', $item_id)->delete();
        }
        $class = static::classThumb($type);
        $thumb = $class::factory($model_id, $item_id);
        $thumb->delete();
        
        return [
            'result'  => 'success',
            'message' => 'Миниатюры успешно удалены!',
            'class'   => $class,
            'models'  => $this->thumbModels($model, $type),
        ];
    }
    
    function thumb() {
        $content     = [];
        $model_class = static::classModel();
        $model_id    = \Route::input('id');
        $model       = $model_class::findOrFail($model_id);
        foreach($model_class::thumbTypes() as $type) {
            $class     = static::classThumb($type);
            $content[] = view('larakit::!.crud.lists.thumb', [
                'url_upload' => route($this->routeBase() . '.id.thumb-upload', \Route::input('id')),
                'url_base'   => route($this->routeBase()),
                'type'       => $type,
                'name'       => $class::getName(),
                'model'      => $model,
            ]);
        }
        
        return \BootstrapDialog\BootstrapDialog::factory()
            ->setTypePrimary()
            ->setTitle('Управление иллюстрациями модели')
            ->setSizeWide()
            ->setNl2br(false)
            ->setMessage('<div class="row">' . implode('', $content) . '</div>')
            ->setDataItem('key', md5($model_class))
            ->setClosable(true)
            ->addButton(
                BootstrapDialogButton::factory()
            )
            ->send();
    }
}