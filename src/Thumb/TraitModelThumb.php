<?php
namespace Larakit\Thumb;

use App\Thumbs\RecommendThumb;
use Larakit\Event\Event;
use Larakit\Models\LarakitModel;

/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 31.07.16
 * Time: 16:00
 * @mixin \Larakit\CRUD\TraitEntity
 * @mixin \Eloquent
 * @mixin LarakitModel
 */
trait TraitModelThumb {

    public static function bootTraitModelThumb() {
        Event::listener(LarakitModel::EVENT_JS_FILTER_ATTRIBUTE, function (\sfEvent $e, $default) {
            $model = $e->getSubject();
            foreach(static::thumbTypes() as $type) {
                $default[] = 'thumb_' . $type . '_' . $model->id;
            }

            return $default;
        });
    }

    static function thumbTypes() {
        return [
            'default',
        ];
    }

    /**
     * @param      $type
     * @param null $item_id
     *
     * @return Thumb
     */
    function thumb($type = 'default', $item_id = null) {
        $class = static::classThumb($type);

        return $class::factory($this->id, $item_id);
    }

    function thumbKey($type = 'default', $item_id = null, $size = '_') {
        return $this->thumb($type, $item_id)->getKey($size);
    }
}