<?php
namespace Larakit\Thumb;

use App\Thumbs\RecommendGroupDocsThumbs;
use App\Thumbs\RecommendGroupGroupThumbs;
use BootstrapDialog\BootstrapDialogButton;
use Illuminate\Http\UploadedFile;
use Larakit\CRUD\TraitEntity;
use Larakit\Models\LarakitImage;
use Larakit\Models\LarakitModel;
use Larakit\Models\LarakitThumbs;
use Larakit\QuickForm\LaraForm;

/**
 * Class TraitControllerThumbses
 * @package Larakit\Thumbs
 * @mixin TraitEntity
 */
trait TraitControllerThumbs {

    function thumbsEdit() {
        $id          = \Request::input('id');
        $textarea_id = \Request::get('textarea_id');
        $image_model = LarakitImage::findOrFail($id);
        $type        = \Request::input('type');
        /** @var LarakitModel $model_class */
        $model_class = $image_model->imageable_type;

        $model = $model_class::findOrFail($image_model->imageable_id);
        $thumb = $model->thumb($type, $image_model->id);
        $form  = new LaraForm('thumbs_item');
        $form->addHidden('id');
        $form->addHidden('type');
        $form->addHidden('textarea_id');
        $form->putTextTwbs('name')->setLabel('Название иллюстрации')
             ->setAppend('.' . $thumb->getExt())
             ->setDesc('Рекомендуется писать на русском языке, при скачивании будет сделан транслит')
             ->ruleRequired();
        $form->initValues($image_model->toArray());
        $form->initValues(compact('type', 'textarea_id'));

        if(\Request::isMethod('post')) {
            if($form->validate()) {
                $image_model->name = \Request::input('name');
                $image_model->save();

                return [
                    'result'  => 'success',
                    'message' => 'Иллюстрация успешно обновлена',
                    'models'  => [
                        [
                            'model' => $image_model->toArray(),
                            'rows'  => [
                                'thumbs'         => (string) view('larakit::!.crud_row.thumbs_row', [
                                    'model' => $image_model,
                                    'type'  => $type,
                                ]),
                                'thumbs_wysiwyg' => (string) view('larakit::!.crud_row.thumbs_row_wysiwyg', [
                                    'model'       => $image_model,
                                    'type'        => $type,
                                    'textarea_id' => $textarea_id,
                                ]),
                            ],
                            'id'    => $image_model->id,
                        ],
                    ],
                ];
            } else {
                return [
                    'result'  => 'error',
                    'message' => 'Все пропало!!!',
                    'body'    => $form . '',
                ];
            }
        }
        $model_class = static::classModel();

        return \BootstrapDialog\BootstrapDialog::factory()
                                               ->setTypePrimary()
                                               ->setTitle('Управление иллюстрацией')
                                               ->setNl2br(false)
                                               ->setMessage($form)
                                               ->setClosable(true)
                                               ->setDataItem('id', $id)
                                               ->setDataItem('key', md5($model_class . $image_model->type))
                                               ->addButton(
                                                   BootstrapDialogButton::factory('Сохранить')
                                                                        ->setActionFile(base_path('vendor/larakit/lk-admin/src/javascripts/attach.js'))
                                               )
                                               ->send();
    }

    function thumbsUpload() {
        $f           = \Request::file('file');
        $type        = \Request::input('type');
        $model_id    = \Route::input('id');
        $textarea_id = \Request::get('textarea_id');
        /* @var $f UploadedFile */
        $model_class = static::classModel();
        $model_thumb = LarakitImage::create([
            'name'           => mb_substr($f->getClientOriginalName(), 0, -1 - mb_strlen($f->getClientOriginalExtension())),
            'type'           => $type,
            'imageable_id'   => $model_id,
            'imageable_type' => $model_class,
        ]);
        /* @var $model LarakitImage */
        $model = $model_class::findOrFail($model_id);
        try {
            /* @var $thumbs \Larakit\Thumb\Thumb */
            $thumb = $model->thumb($type, $model_thumb->id);
            $thumb->processing($f);
            $model_thumb->imageable()->associate($model);
            $model_thumb->save();

            return [
                'result'  => 'success',
                'message' => 'Файл  "' . $f->getClientOriginalName() . '" успешно загружен!',
                'models'  => [
                    [
                        'model' => $model_thumb->toArray(),
                        'rows'  => [
                            'thumbs'         => (string) view('larakit::!.crud_row.thumbs_row', [
                                'model' => $model_thumb,
                                'type'  => $type,
                            ]),
                            'thumbs_wysiwyg' => (string) view('larakit::!.crud_row.thumbs_row_wysiwyg', [
                                'model'       => $model_thumb,
                                'type'        => $type,
                                'textarea_id' => $textarea_id,
                            ]),
                        ],
                        'id'    => $model_thumb->id,
                    ],
                ],
            ];
        }
        catch(\Exception $e) {
            $model_thumb->delete();
        }
    }

    function thumbsWysiwygInsert() {
        $content     = [];
        $model_class = static::classModel();
        $model_id    = \Route::input('id');
        $textarea_id = \Request::get('textarea_id');
        $model       = $model_class::findOrFail($model_id);
        $type        = 'illustration';
        $class       = static::classThumb($type);
        $content[]   = view('larakit::!.crud_row.thumbs-wysiwyg', [
            'url_upload'  => route($this->routeBase() . '.id.thumbs-upload', \Route::input('id')),
            'url_base'    => route($this->routeBase()),
            'type'        => $type,
            'name'        => $class::getName(),
            'key'         => md5($model_class . $type),
            'textarea_id' => $textarea_id,
            //                'thumb_key'        => $class::factory($model_id,)->getKey('_'),
            'thumbs'      => $model->larakitImages()->where('type', '=', $type)->get(),
        ]);

        return \BootstrapDialog\BootstrapDialog::factory()
                                               ->setTypePrimary()
                                               ->setTitle('Управление галереями')
                                               ->setSizeWide()
                                               ->setNl2br(false)
                                               ->setMessage(implode('', $content))
                                               ->setClosable(true)
                                               ->addButton(
                                                   BootstrapDialogButton::factory('Закрыть')
                                                                        ->addClass('js-modal-close')
                                               )
                                               ->send();
    }

    function thumbsTypes() {
        return [
        ];
    }

    function thumbs() {
        $content     = [];
        $model_class = static::classModel();
        $model_id    = \Route::input('id');
        $model       = $model_class::findOrFail($model_id);
        foreach($this->thumbsTypes() as $type) {
            $class     = static::classThumb($type);
            $content[] = view('larakit::!.crud_row.thumbs', [
                'url_upload' => route($this->routeBase() . '.id.thumbs-upload', \Route::input('id')),
                'url_base'   => route($this->routeBase()),
                'type'       => $type,
                'name'       => $class::getName(),
                'key'        => md5($model_class . $type),
                //                'thumb_key'        => $class::factory($model_id,)->getKey('_'),
                'thumbs'     => $model->larakitImages()->where('type', '=', $type)->get(),
            ]);
        }

        return \BootstrapDialog\BootstrapDialog::factory()
                                               ->setTypePrimary()
                                               ->setTitle('Управление галереями')
                                               ->setSizeWide()
                                               ->setNl2br(false)
                                               ->setMessage(implode('', $content))
                                               ->setClosable(true)
                                               ->addButton(
                                                   BootstrapDialogButton::factory('Закрыть')
                                               )
                                               ->send();

    }

    function thumbsDelete() {
        $model_class         = static::classModel();
        $type                = \Request::input('type');
        $model_id            = \Request::input('id');
        $item_id             = \Request::input('item_id');
        $larakit_image_model = LarakitImage::findOrFail($item_id);
        $class               = static::classThumb($type);
        $thumb               = $class::factory($model_id, $item_id);
        $thumb->delete();
        $larakit_image_model->delete();

        return [
            'result'  => 'success',
            'message' => 'Миниатюры успешно удалены!',
        ];
    }

}