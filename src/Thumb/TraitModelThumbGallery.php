<?php
namespace Larakit\Thumb;

use Larakit\Models\LarakitImage;

/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 31.07.16
 * Time: 16:00
 * @mixin \Larakit\CRUD\TraitEntity
 * @mixin \Eloquent
 */
trait TraitModelThumbGallery {

    /**
     * @param $type
     *
     * @return Thumb
     */
    function thumbsClass($type) {
        return static::getVendorStudly() . '\Thumbs\\' . self::getEntityStudly() . \Illuminate\Support\Str::studly($type) . 'Thumbs';
    }

    function thumbGallery($type, $id = 0) {
        if($id) {
            $image = LarakitImage::find($id);
        } else {
            $image = LarakitImage::create([
                'type' => $type,
            ]);
            $image->imageable()->associate($this);
            $image->save();
        }
        $class = static::thumbsClass($type);

        return $class::fromModel($this, $type, $image->id);
    }

    /**
     * @param     $type
     * @param int $id
     *
     * @return Thumb
     */
    function uploadThumb($type, $id = 0) {

        return Thumb::fromModel($this, $type, $image->id);
    }

    function thumbItemRemove() {
    }

    function thumbItemReplace() {
    }

    public function uploadThumbSm($id) {
        return $this->uploadThumb('sm', $id);
    }

    public function larakitImages() {
        return $this->morphMany(LarakitImage::class, 'imageable');
    }

    public function larakitImagesSm() {
        return $this->larakitImages()->where('type', '=', 'sm');
    }

}