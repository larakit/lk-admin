<?php
namespace Larakit\Thumb;

use Larakit\Models\LarakitImage;

/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 31.07.16
 * Time: 16:00
 * @mixin \Larakit\CRUD\TraitEntity
 * @mixin \Eloquent
 */
trait TraitModelThumbs {

    public function larakitImages() {
        return $this->morphMany(LarakitImage::class, 'imageable');
    }
}