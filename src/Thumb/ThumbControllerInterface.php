<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 04.08.16
 * Time: 9:51
 */

namespace Larakit\Thumb;

interface ThumbControllerInterface {

//    function attachTypes() {
//        return [
//            'docs',
//            'group',
//        ];
//    }
//
//    function routeBase() {
//        return 'admin.recommend_group';
//    }

    /**
     * @return array
     */

    function thumbTypes();

    /**
     * @return string
     */
    function routeBase();

}