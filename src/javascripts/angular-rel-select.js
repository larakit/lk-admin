angular.module('RelSelect', ['larakit']);
angular.module('RelSelect').controller('Ctrl', function ($scope, $rootScope) {
    $scope.data = {};
    $scope.items = "{items}";
    $scope.model = "{model}";
    $scope.data.selected = "{selected}";
    $scope.data.relation = "{relation}";
    $scope.max = "{max}";
    $scope.getAllWithoutSelected = function () {
        return _.filter(
            $scope.items,
            function (o) {
                return -1 === $scope.data.selected.indexOf(o.id);
            }
        );
    };
    $scope.getSelected = function () {
        return _.filter(
            $scope.items,
            function (o) {
                return -1 !== $scope.data.selected.indexOf(o.id);
            }
        );
    };

    $scope.selectRow = function (id) {
        if ($scope.max && $scope.max <= $scope.data.selected.length) {
            toastr.error('Превышено допустимое количество выбранных элементов');
            return false;
        }
        $scope.data.selected.push(id);
    };

    $scope.unSelectRow = function (id) {
        var index = $scope.data.selected.indexOf(id);
        if (-1 !== index) {
            $scope.data.selected.splice(index, 1);
        }
    };


    $rootScope.getData = function () {
        return $scope.data;
    };

});
angular.bootstrap(dialog.getModalBody(), ['RelSelect']);