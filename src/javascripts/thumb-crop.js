var url = dialog.getData('url'),
    postData = {
        cropper: dialog.getModalBody().find('.js-crop-img').cropper('getData'),
        type: dialog.getData('type'),
        size: dialog.getData('size'),
        id: dialog.getData('id'),
        item_id: dialog.getData('item_id')
    };
// thumbSize_image = dialog.getModalBody().find('.js-cropper-source img');
$.post(url,
    postData,
    function (data) {
        if ('success' == data.result) {
            $('.js-thumb[data-thumb-key=' + data.thumb_key + ']')
                .attr('src', data.url)
                .css('height', 'auto');
            dialog.close();

        }
        larakit_toastr(data);
    }
);
