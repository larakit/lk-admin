var body = dialog.getModalBody(),
    form = body.find('form'),
    url = form.attr('action');
$.post(url, form.serialize(), function (response) {
    if ('success' == response.result) {
        dialog.close();
        LarakitManager.rows(dialog.getData('key'), response.models);
    } else {
        body.html(response.body);
    }
    larakit_toastr(response);
});