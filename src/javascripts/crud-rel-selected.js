var body = dialog.getModalBody(),
    form = body.find('form'),
    url = form.attr('action'),
    data = angular.element(body).scope().getData();
$.post(url, data, function (response) {
    if ('success' == response.result) {
        dialog.close();
        $(response.selector).html(response.html);
        LarakitJs.fire();
    } else {
        if (response.body) {
            body.html(response.body);
        }
    }
    larakit_toastr(response);
});
