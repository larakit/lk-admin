var body = dialog.getModalBody(),
    form = body.find('form'),
    url = form.attr('action'),
    data;
if(form.hasClass('angular-form')){
    data = angular.element(body).scope().getData();
}else {
    data = form.serialize();
}
$.post(url, data, function (response) {
    if ('success' == response.result) {
        dialog.close();
        LarakitManager.rows(dialog.getData('key'), response.models);
        LarakitJs.fire();
        var callback = dialog.getData('callback');
        if(undefined!=callback){
            callback.call(dialog, response);
        }
    } else {
        if(response.body){
            body.html(response.body);
        }
    }
    larakit_toastr(response);
});
