<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 19.05.16
 * Time: 20:10
 */

namespace Larakit\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;

class MakeController extends GeneratorCommand {

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new controller class';
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'larakit:make:controller';
    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Controller';

    /**
     * Get the default namespace for the class.
     *
     * @param  string $rootNamespace
     *
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace) {
        return $rootNamespace . '\Http\Controllers';
    }

    protected function getNameInput() {
        return Str::studly('admin_' . parent::getNameInput() . '_controller');
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub() {
        return __DIR__ . '/stubs/controller.stub';
    }

    protected function replaceClass($stub, $name) {
        $class = str_replace($this->getNamespace($name) . '\\', '', $name);

        $stub = str_replace('{ROUTE}', strtoupper(Str::snake(parent::getNameInput())), $stub);

        return str_replace('DummyClass', $class, $stub);
    }

}
