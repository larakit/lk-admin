<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 19.05.16
 * Time: 20:10
 */

namespace Larakit\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Larakit\Event\Event;
use Symfony\Component\Console\Input\InputArgument;

class Make extends Command {

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new crud-module';
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'larakit:make';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        $name = Str::studly($this->argument('name'));
        $args = ['name' => $name];
        $this->call('larakit:make:quickform', $args);
        $this->call('larakit:make:attach', $args);
        $this->call('larakit:make:boot', $args);
        $this->call('larakit:make:controller', $args);
        $this->call('larakit:make:formfilter', $args);
        $this->call('larakit:make:thumb', $args);
        $this->call('larakit:make:thumbs', $args);
        $this->call('larakit:make:validator', $args);
        $this->call('larakit:make:model', $args);
        $this->call('larakit:make:view', $args);
        $this->call('make:migration', [
            'name'    => 'create_' . Str::plural(Str::snake($name)) . '_table',
            '--create' => Str::plural(Str::snake($name)),
        ]);
        Event::notify('larakit:make', $args);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return [
            ['name', InputArgument::REQUIRED, 'Имя модели вида "RecommendGroup" или "recommend_group"'],
        ];
    }

}
