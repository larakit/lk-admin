<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 19.05.16
 * Time: 20:10
 */

namespace Larakit\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;

class MakeBoot extends GeneratorCommand {

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new boot file';
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'larakit:make:boot';
    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'BOOT.php';

    /**
     * Get the default namespace for the class.
     *
     * @param  string $rootNamespace
     *
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace) {
        return $rootNamespace . '\Http\boot';
    }

    protected function getPath($name) {
        return base_path('routes/web/' . $this->getNameInput() . '.php');
    }

    protected function getNameInput() {
        return 'admin.' . Str::snake(parent::getNameInput());
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub() {
        return __DIR__ . '/stubs/boot.stub';
    }

    protected function replaceClass($stub, $name) {
        $class = str_replace($this->getNamespace($name) . '\\', '', $name);

        $stub = str_replace('{MODEL_SNAKE_UPPER}', strtoupper(Str::snake(parent::getNameInput())), $stub);
        $stub = str_replace('{MODEL_SNAKE}', Str::snake(parent::getNameInput()), $stub);
        $stub = str_replace('{MODEL_SNAKE_DASH}', Str::snake(parent::getNameInput(), '-'), $stub);

        return str_replace('DummyClass', $class, $stub);
    }

}
