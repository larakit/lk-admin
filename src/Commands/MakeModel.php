<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 19.05.16
 * Time: 20:10
 */

namespace Larakit\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;

class MakeModel extends GeneratorCommand {

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new model class';
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'larakit:make:model';
    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Model';

    /**
     * Get the default namespace for the class.
     *
     * @param  string $rootNamespace
     *
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace) {
        return $rootNamespace . '\Models';
    }

    protected function getNameInput() {
        return Str::studly(parent::getNameInput());
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub() {
        return __DIR__ . '/stubs/model.stub';
    }

    protected function replaceClass($stub, $name) {
        $class = str_replace($this->getNamespace($name) . '\\', '', $name);

        $stub = str_replace('{table_name}', Str::snake(Str::plural(parent::getNameInput())), $stub);

        return str_replace('DummyClass', $class, $stub);
    }

}