<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 19.05.16
 * Time: 20:10
 */

namespace Larakit\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputArgument;

class MakeView extends Command {

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new view file';
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'larakit:make:view';

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return [
            ['name', InputArgument::REQUIRED, 'Имя модели вида "RecommendGroup" или "recommend_group"'],
        ];
    }

    public function fire() {
        $name = Str::snake($this->argument('name'));
        $dest = resource_path('views/admin/'.$name.'.twig');
        if(!file_exists($dest)){
            $dir = dirname($dest);
            if(!is_dir($dir)){
                mkdir($dir, 0777, true);
            }
            copy($this->getStub(), $dest);
        } else {
            $this->error('View already exists!');
        }

    }

        /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub() {
        return __DIR__ . '/stubs/view.stub';
    }

}
